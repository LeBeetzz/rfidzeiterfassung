import MySQLdb

class SQLStatements(object):
	__user = "root"
	__password = "abcD123"
	sqlUrl = "localhost"
	db = "raspi"

	def findUserNameForCardId(self, cardId):
		query = """
			SELECT u.vorname, u.nachname
			FROM raspi_card c
			INNER JOIN raspi_user u ON c.user_id = u.user_id
			WHERE c.card_id = %s
	    """ % (cardId)

		db = MySQLdb.connect(host=self.sqlUrl, user = self.__user, passwd = self.__password, db = self.db)
		cur = db.cursor()
		cur.execute(query)

		firstName = None
		lastName = None
		for row in cur.fetchall():
			firstName = row[0]
			lastName = row[1]

		result = None
		if firstName != None and lastName != None:
			result = firstName +" "+ lastName

		db.close()
		return result

	def findUserIdForCardId(self, cardId):
		query = """
	    			SELECT user_id
	    			FROM raspi_card
	    			WHERE card_id = %s
	    	    """ % (cardId)

		db = MySQLdb.connect(host=self.sqlUrl, user=self.__user, passwd=self.__password, db=self.db)
		cur = db.cursor()
		cur.execute(query)

		for row in cur.fetchall():
			userId = row[0]

		db.close()
		return userId

	def saveTimeCommit(self, userId, type):
		query = """
			INSERT INTO raspi_time (user_id, type_id, zeitpunkt) VALUES (%s,%s,SYSDATE())
		""" % (userId,type)
		db = MySQLdb.connect(host=self.sqlUrl, user=self.__user, passwd=self.__password, db=self.db)
		cur = db.cursor()
		cur.execute(query)
		db.commit()
		db.close()

	def findLastCommitTypeForUser(self, userId):
		query = """
			SELECT type_id
			FROM raspi_time
			WHERE user_id = %s
			ORDER BY zeitpunkt DESC
			LIMIT 1
		""" % (userId)

		db = MySQLdb.connect(host=self.sqlUrl, user=self.__user, passwd=self.__password, db=self.db)
		cur = db.cursor()
		cur.execute(query)

		type = 0
		for row in cur.fetchall():
			type = row[0]
		db.close()

		return type


