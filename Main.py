import lcddriver
import RPi.GPIO as GPIO
import MFRC522
import signal
import time
from SQLStatements import SQLStatements
from Processor import Processor

continue_scan = True;
lcd = lcddriver.lcd()
scaner = MFRC522.MFRC522()

GPIO.setwarnings(False)

def authCard(uid):
	key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

	scaner.MFRC522_SelectTag(uid)

	status = scaner.MFRC522_Auth(scaner.PICC_AUTHENT1A,8, key, uid)

	if status == scaner.MI_OK:
		return True
	else:
		return False

def mergeUid(uid):
	result = ""

	for part in uid:
		result = result + str(part)

	return result


def destroy():
	continue_scan = False
	GPIO.cleanup()
	lcd.lcd_clear()

while continue_scan:
	lcd.lcd_clear()
	lcd.lcd_display_string("Bereit", 1)

	(status, TagType) = scaner.MFRC522_Request(scaner.PICC_REQIDL)

	if status == scaner.MI_OK:
		lcd.lcd_clear()
		lcd.lcd_display_string("Karte",1)
		lcd.lcd_display_string("gefunden",2)

		(status, uid) = scaner.MFRC522_Anticoll()

		if status == scaner.MI_OK:
			if authCard(uid):
				lcd.lcd_clear()
				lcd.lcd_display_string("Karte", 1)
				lcd.lcd_display_string("verfiziert", 2)
				time.sleep(1)

				uid = mergeUid(uid)
				lcd.lcd_clear()
				lcd.lcd_display_string("Card ID:",1)
				lcd.lcd_display_string(uid,2)
				time.sleep(2)

				sql = SQLStatements()
				user = sql.findUserNameForCardId(uid)

				if user == None:
					lcd.lcd_clear()
					lcd.lcd_display_string("Fehler",1)
					time.sleep(2)
				else:
					lcd.lcd_clear()
					lcd.lcd_display_string(user, 1)
					time.sleep(3)

					processor = Processor()
					type = processor.commitTimeBooking(uid)

					lcd.lcd_clear()
					if type == 1:
						lcd.lcd_display_string("Eingang",1)
					elif type == 2:
						lcd.lcd_display_string("Ausgang", 1)
					lcd.lcd_display_string("gebucht!",2)
					time.sleep(3)

				scaner.MFRC522_StopCrypto1()
			else:
				lcd.lcd_clear()
				lcd.lcd_display_string("Karte", 1)
				lcd.lcd_display_string("ungueltig", 2)

		else:
			lcd.lcd_clear()
			lcd.lcd_display_string("Fehler",1)
			lcd.lcd_display_string("beim Lesen!",2)
			time.sleep(1)

destroy()
