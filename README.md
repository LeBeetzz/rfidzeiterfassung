<h3>RFID Zeiterfassung</h3>
<br>
Dies ist der Quellcode der Themenwoche "RaspberryPi - RFID Sensoren".<br>
Das erstellte Programm ist eine Zeiterfassung. Hierfür wurde eine Datenbank erstellt. Des Weiteren wurden RFID Chips beschrieben und in der Datenbank einzelnen Nutzern zugewiesen.<br>
<br>
Beim annähern des Chips an den Sensoren wird der jeweilige Nutzer im Zeiterfassungssystem ein bzw. ausgebucht.