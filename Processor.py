from SQLStatements import SQLStatements
class Processor:

    __sql = SQLStatements()

    def commitTimeBooking(self, cardId):
        userId = self.__findUserId(cardId)
        type = self.__findTypeForUser(userId)
        self.__sql.saveTimeCommit(userId, type)

        return type

    def __findUserId(self, cardId):
        return self.__sql.findUserIdForCardId(cardId)

    def __findTypeForUser(self, userId):
        currentType = self.__sql.findLastCommitTypeForUser(userId)
        typeToSave = None

        if currentType == None:
            typeToSave = 1
        elif currentType == 1:
            typeToSave = 2
        elif currentType == 2:
            typeToSave = 1

        return typeToSave

